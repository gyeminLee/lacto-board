# RockTo Board

## 개발환경 준비

### NodeJS 설치
[node.js download](https://nodejs.org/)
* v8.9.4

### Bower 설치
 npm install -g bower
### Gulp 설치 
 npm install -g gulp
### Git 설치
 버전관리 시스템으로 Git을 사용
### NPM 패키지 설치
 npm install
## 테스팅(e2e)
- Angular Js 어플리케이션을 위한 end-to-end test framework
- 실제 브라우저에서 실행중인 응요프로그램에 대한 테스트를 실행
 gulp test
 